
# 参数类型，默认长度

---


<table>
    <tr>
        <th>接口名</th>
        <td>param.type.9</td>
        <th>版本号</th>
        <td></td>
    </tr>
</table>

**请求参数**

<table>
    <tr>
        <th>名称</th>
        <th>类型</th>
        <th>是否必须</th>
        <th>最大长度</th>
        <th>示例值</th>
        <th>描述</th>
    </tr>
        <tr><td>goods_name</td><td class="param-type">string</td><td><strong>是</strong></td><td>64</td><td>iphoneX</td><td>自定义长度</td></tr>
        <tr><td>remark</td><td class="param-type">string</td><td>否</td><td></td><td></td><td>备注</td></tr>
        <tr><td>b</td><td class="param-type">boolean</td><td>否</td><td>2</td><td></td><td>b</td></tr>
        <tr><td>aByte</td><td class="param-type">byte</td><td>否</td><td>4</td><td></td><td>byte</td></tr>
        <tr><td>aShort</td><td class="param-type">short</td><td>否</td><td>6</td><td></td><td>short</td></tr>
        <tr><td>integer</td><td class="param-type">integer</td><td>否</td><td>11</td><td></td><td>i</td></tr>
        <tr><td>aLong</td><td class="param-type">long</td><td>否</td><td>20</td><td></td><td>l</td></tr>
        <tr><td>aFloat</td><td class="param-type">float</td><td>否</td><td>11</td><td></td><td>f</td></tr>
        <tr><td>aDouble</td><td class="param-type">double</td><td>否</td><td></td><td></td><td>double</td></tr>
    </table>

**参数示例**

```json
{
	"goods_name":"iphoneX",
	"b":"",
	"aShort":"",
	"aLong":"",
	"aFloat":"",
	"remark":"",
	"aDouble":"",
	"integer":"",
	"aByte":""
}
```

**返回结果**

<table>
    <tr>
        <th>名称</th>
        <th>类型</th>
        <th>描述</th>
    </tr>
    <tr>
        <td>code</td>
        <td>string</td>
        <td>状态值，"0"表示成功，其它都是失败</td>
    </tr>
    <tr>
        <td>msg</td>
        <td>string</td>
        <td>错误信息，出错时显示</td>
    </tr>
        <tr>
        <td>data</td>
        <td>object</td>
        <td>返回的数据，没有则返回{}
            <table>
                <tr>
                    <th>名称</th>
                    <th>类型</th>
                    <th>最大长度</th>
                    <th>示例值</th>
                    <th>描述</th>
                </tr>
                            </table>
        </td>
    </tr>
    </table>

**返回示例**

```json
{
	"code":"0",
	"data":{},
	"msg":""
}
```


